import numpy as np
import numpy.linalg as linalg
import pandas
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import scipy.optimize as optimize
#rom SALib.sample import saltelli
#from SALib.analyze import sobol
import csv
import numpy as np
import matplotlib.pyplot as plt

tps_etude_model=150
gamma=0.120

pop_da = 5460
S0 = pop_da-2
I0= 2
R0 = 0
D0=0
Y0=[S0,I0,R0,D0]
nb_blocks = 8


file=np.loadtxt("data_cas.txt")
jour=file[:,0]
da=file[:,1]
deces=file[:,2]
xi=file[:,2]

#%% 1er modèle : on enlève Xiao et on regarde ce qui se passe pour Da
def deriv_donnes_1_Da(Y, t,beta, mu):
    '''
    On fit beta beta et mu
    '''
    S, I, R, D = Y
    dS = - beta*S*I/(S+I+R)
    dI = beta*S*I/(S+I+R) - gamma*I - mu * I
    dR = gamma * I
    dD = mu*I
    return ([dS, dI, dR, dD])




def squared_differences(params_to_fit, times):
    beta_fit, mu_fit= params_to_fit

    params = (beta_fit, mu_fit) 
    model_predictions = integrate.odeint(deriv_donnes_1_Da, Y0, times, args=params)
    

    # le résultat de la fonction squared_differences doit être un array de dimension 1 :
    # il faut donc concaténer dans un même array les différences au carré sur les différents outputs de votre modèle (si il y en a plusieurs)
    # on pourra aussi normaliser les différents outputs pour un meilleur résultat (par exemple si la population susceptible >> la population infectée)
    diff = np.array([])
    for k in range(len(jour)):
        diff = np.append(diff,da[k]-model_predictions[int(jour[k]),2])
    for k in range(len(jour)):
        diff = np.append(diff,deces[k]-model_predictions[int(jour[k]),3])
    return diff

def plot_donnes1_Da():
    initial_params = [0.3,0.02]
    fitted_params = optimize.leastsq(squared_differences, initial_params, args=(np.linspace(0,62,62+1)))

    print("Initial parameters : " + str(initial_params))
    print("Parameters after fitting : " + str(fitted_params[0]))

    identif_params = (fitted_params[0][0], fitted_params[0][1])
    courbe_identif = integrate.odeint(deriv_donnes_1_Da, Y0, np.linspace(0,tps_etude_model,tps_etude_model+1), args=identif_params)

    S = courbe_identif[:,0]
    I = courbe_identif[:,1]
    R = courbe_identif[:,2]
    D = courbe_identif[:,3]
    time_model=np.linspace(0,tps_etude_model,tps_etude_model+1)

    plt.plot(time_model,S,label="S")
    plt.plot(time_model,I,label="I")
    plt.plot(time_model,R,label="R")
    plt.plot(time_model,D,label="D")
    plt.grid()
    plt.legend()


plt.show()

#%% On prend les même paramètres pour Xiao et on regarde ce qui se passe : (pas de fitting, hypothèse : la maladie se propage avec les memes paramètres pour Da et Xiao)
beta = 0.24169
mu= 0.03527


def deriv_donnes1_da_xiao(Y, t):
    SD, ID, RD,DD , SX, IX, RX, DX = Y
    ND = SD + ID + RD
    NX = SX + IX + RX
    T = 100
    if t%7 == 0:
        T= 200
    dSD = - beta*SD*ID/ND
    dID = beta*SD*ID/ND - (gamma + mu)*ID 
    dRD = gamma*ID
    dDD = mu*ID
    dSX = - beta*(SX/NX)*(IX+(T*ID/ND))
    dIX = beta*(SX/NX)*(IX+(T*ID/ND)) - (gamma+mu)*IX
    dRX = gamma*IX
    dDX = mu*IX
    return ([dSD, dID, dRD, dDD, dSX, dIX, dRX, dDX])

Y0 = [pop_da-2,2,0,0,550,0,0,0]

def plot_donnes1_final(Y0):
    courbe_identif = integrate.odeint(deriv_donnes1_da_xiao, Y0, np.linspace(0,tps_etude_model,tps_etude_model+1))
    tracer=[courbe_identif[:,k] for k in range(nb_blocks)]
    name = ["SD","ID","RD","DD","SX","IX","RX","DX"]
    for k in range(0,len(name)):
        plt.plot(np.linspace(0,tps_etude_model,tps_etude_model+1),tracer[k],label=name[k])
    plt.title("Simulation du modèle")
    plt.xlabel("Jours")
    plt.ylabel("Nb de personnes")
    plt.legend()

    plt.show()

#plot_donnes1_final(Y0)

#%% On continue de fitter pour les données 2 et 3, ici on ne donne que les valeurs finales des paramètres car l'algorithme de fit a déjà été présenté plus haut.

tps_etude_model=200
tps_donnees = 110
nb_param = 2
nb_blocks = 10

SD0 = 5458 
ED0 = 8 # A CHECKER ! 
ID0 = 2
RD0 = 0
DD0 = 0

SX0 = 5458 
EX0 = 0 # A CHECKER ! 
IX0 = 0
RX0 = 0
DX0 = 0

Y0 = [SD0,ED0, ID0, RD0, DD0,SX0,EX0,IX0,RX0,DX0]
# On fitte les paramètres delta, et beta_manif (on suppose que delta, beta et mu sont caractéristiques de la maladie et ne varient pas)
initial_params = [0.5]
beta = 0.24169
gamma=0.120
mu= 0.03527
delta=0.25

#avant manif : 
beta = 0.248
delta = 0.295
gamma = 0.103
mu = 0.02

#pendant manif 
beta_manif = 0.28
delta_manif_da = 0.15
delta_manif_xiao = 0.40
gamma_manif = 0.109
mu_manif = 0.02


# Recuperation des données : 

file=np.loadtxt("data2.txt",dtype=str)
jour2=file[:,0]

jour=jour2.astype(float)
    
da=file[:,1]
for k in range (len(da)):
    if da[k]=='NA':
        da[k]=-1
da=da.astype(float)

decesDA=file[:,2]
for k in range (len(decesDA)):
    if decesDA[k]=='NA':
        decesDA[k]=-1
decesDA=decesDA.astype(float)

xi=file[:,3]
for k in range (len(xi)):
    if xi[k]=='NA':
        xi[k]=-1
xi=xi.astype(float)

decesXI=file[:,4]
for k in range (len(decesXI)):
    if decesXI[k]=='NA':
        decesXI[k]=-1
decesXI=decesXI.astype(float)

guerDA=file[:,5]
for k in range (len(guerDA)):
    if guerDA[k]=='NA':
        guerDA[k]=-1

guerDA=guerDA.astype(float)

guerXI=file[:,6]
for k in range (len(guerXI)):
    if guerXI[k]=='NA':
        guerXI[k]=-1
guerXI=guerXI.astype(float)

'''
Au final on a les listes suivantes :
jour, da, xi, decesXI, decesDA, guerDA, guerXI
'''



def deriv_donnes2_da(Y, t):
    SD, ED, ID, RD,DD,SX,EX,IX,RX,DX = Y
    ND = SD + ED +  ID + RD
    NX = SX+EX+IX+RX
    T = 100
    if int(t)%7 == 0:
        T= 200
    if t>=101 and t<=111:
        dSD = - beta_manif*SD*ID/ND
        dED = beta_manif*SD*ID/ND - delta_manif_da*ED
        dID = delta_manif_da*ID -(gamma_manif + mu_manif)*ID 
        dRD = gamma_manif*ID
        dDD = mu_manif*ID
        dSX = - (beta_manif*IX*SX/NX)-(beta_manif)*T*SX*ID/(NX*ND)
        dEX = (beta_manif*IX*SX/NX)+(beta_manif)*T*SX*ID/(NX*ND) - delta_manif_xiao*EX
        dIX = delta_manif_xiao*EX - (gamma_manif+mu_manif)*IX
        dRX = gamma_manif*IX
        dDX = mu_manif*IX
    else: 
        dSD = - beta*SD*ID/ND
        dED = beta*SD*ID/ND - delta*ED
        dID = delta*ED -(gamma + mu)*ID 
        dRD = gamma*ID
        dDD = mu*ID
        dSX = - (beta*IX*SX/NX)-(beta)*T*SX*ID/(NX*ND)
        dEX = (beta*IX*SX/NX)+(beta)*T*SX*ID/(NX*ND) - delta*EX
        dIX = delta*EX - (gamma+mu)*IX
        dRX = gamma*IX
        dDX = mu*IX
    
    
    return ([dSD,dED, dID, dRD, dDD, dSX,dEX, dIX, dRX, dDX])



def squared_differences(params_to_fit, times):

    '''
    L'algorithme utilisé pour faire fitter nos données à chaque étape de notre raisonnement. Ici il n'est pas utile car nous avons fixé tous nos paramètres grace à cet algorithme
    '''
    beta_manif = params_to_fit


    params = (beta_manif) 
    model_predictions = integrate.odeint(deriv_donnes2_da, Y0, times, args=tuple([beta_manif]))
    

    # le résultat de la fonction squared_differences doit être un array de dimension 1 :
    # il faut donc concaténer dans un même array les différences au carré sur les différents outputs de votre modèle (si il y en a plusieurs)
    # on pourra aussi normaliser les différents outputs pour un meilleur résultat (par exemple si la population susceptible >> la population infectée)
    diff = np.array([])
    for k in range(len(jour)):
        if da[k]!=-1:
            diff = np.append(diff,da[k]-model_predictions[int(jour[k]),2])
        if decesDA[k]!=-1:
            diff = np.append(diff,decesDA[k]-model_predictions[int(jour[k]),4])
        if guerDA[k]!=-1:
            diff = np.append(diff,guerDA[k]-model_predictions[int(jour[k]),3])
    
    return diff


def plot_model_donnes2_da():
    print(Y0)
    courbe_identif = integrate.odeint(deriv_donnes2_da, Y0, np.linspace(0,tps_etude_model,tps_etude_model+1))
    tracer=[courbe_identif[:,k] for k in range(nb_blocks)]
    name = ["Sains","Contagieux","Infectés","Soignés","Morts","SX","EX","IX","RX","DX"]
    for k in range(0,5):
        plt.plot(np.linspace(0,tps_etude_model,tps_etude_model+1),tracer[k],label=name[k])
    plt.plot(jour,da,label="donnees infectés")
    plt.title("Simulation du modèle")
    plt.xlabel("Semaines")
    plt.ylabel("Nb de personnes")
    plt.legend()

    plt.show()
    '''
    for k in range(5,10):
        plt.plot(np.linspace(0,tps_etude_model,tps_etude_model+1),tracer[k],label=name[k])
    plt.title("Simulation du modèle")
    plt.xlabel("Semaines")
    plt.ylabel("Nb de personnes")
    plt.plot()
    plt.plot(jour,xi,label="donnees infectés")
    plt.legend()
    '''

    plt.show()

plot_model_donnes2_da()